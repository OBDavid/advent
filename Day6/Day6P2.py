file=open('w:/Projects/POS/advent/advent/Day6/input',"r")
lines = file.readlines()
allPlanets=[]

class planet:
      def __init__(self, name, orbit):
        self.name = name
        self.orbit = orbit


def searchPlanet(name):
    for p in allPlanets:
        if p.name==name:
            return p
    return None
def searchOrbitingPlanets(name):
    re=[]
    for p in allPlanets:
            if p.orbit!=None and p.orbit.name==name:
                re.append(p)
    return re

for a in lines:
    planets=a.split(")")
    planets[1]=planets[1].split("\n")[0]
    com=searchPlanet(planets[0])
    op=searchPlanet(planets[1])
    if com==None:
        com=planet(planets[0],None)
        allPlanets.append(com)
    if op==None:
        op=planet(planets[1],com)
        allPlanets.append(op)
    else:
        op.orbit=com        
    print("P1:"+op.name+",Orbit:"+com.name)


you=searchPlanet("YOU")
san=searchPlanet("SAN")

m=0
i=0
youtocom=[]
while you.orbit!=None:
    youtocom.append(you.orbit.name)
    you=you.orbit

while san.orbit!=None:
    if youtocom.count(san.name)>0:
        m=youtocom.index(san.name)+i-1
        break
    san=san.orbit
    i+=1
print("Amount: "+str(m))


file.close()