class point:
    def __init__(self,x,y):
        self.x=x
        self.y=y
        super().__init__()

file=open('W:\\Projects\\POS\\advent\\advent\\Day3\\input',"r")
cables=file.readlines()
    
points=[]
c=0
for cable in cables:
    commands=cable.split(",")
    relativePositionX=0
    relativePositionY=0

    points.append([])
    for command in commands:
        restring=""
        rnd=0
        for a in command:
            if rnd!=0:
                restring=restring+a
            rnd=rnd+1
        commandNumber=int(restring)

        if(command[0]=='U'):
            i=0
            while i<commandNumber:
                p=point(relativePositionX,relativePositionY+i)
                points[c].append(p)
                i=i+1
                #print("x:"+str(p.x)+", y:"+str(p.y))
            relativePositionY=relativePositionY+commandNumber
            print("c:"+str(c)+", x:"+str(relativePositionX)+", y:"+str(relativePositionY))
        elif(command[0]=='D'):
            i=0
            while i<commandNumber:
                p=point(relativePositionX,relativePositionY-i)
                points[c].append(p) 
                i=i+1 
                #print("x:"+str(p.x)+", y:"+str(p.y))
            relativePositionY=relativePositionY-commandNumber
        elif(command[0]=='L'):
            i=0
            while i<commandNumber:
                p=point(relativePositionX-i,relativePositionY)
                points[c].append(p)
                i=i+1                
                #print("x:"+str(p.x)+", y:"+str(p.y))
            relativePositionX=relativePositionX-commandNumber
            
        elif(command[0]=='R'):
            i=0
            while i<commandNumber:
                p=point(relativePositionX+i,relativePositionY)
                points[c].append(p)
                i=i+1
                #print("x:"+str(p.x)+", y:"+str(p.y))
            relativePositionX=relativePositionX+commandNumber
        else:
            print("WutFace")
    c=c+1
i=0
for c in cables:
    print("c"+str(i)+": "+str(len(points[i])))
    i=i+1

def orderByAbs(element):
    return abs(element.x)+abs(element.y)

cableNumber=0
for cable in points:
    print("Ordering Cable "+str(cableNumber))
    points[cableNumber]=sorted(cable,key=orderByAbs)
    print("fin/ probe: "+str(abs(points[cableNumber][0].x)+abs(points[cableNumber][0].y))+", "+str(abs(points[cableNumber][1].x)+abs(points[cableNumber][1].y))+", ... "+str(abs(points[cableNumber][len(points[cableNumber])-1].x)+abs(points[cableNumber][len(points[cableNumber])-1].y)))
    cableNumber=cableNumber+1
import sys
lowestNumb=sys.maxsize

percent=0
counter=0
cross=[]

while counter<len(points[0]):
    if percent!=int(counter*100/len(points[0])):
        percent=int(counter*100/len(points[0]))
        print(str(percent)+"%")
    counter2=0
    while counter2<counter and abs(points[0][counter].x)+abs(points[0][counter].y)>=abs(points[1][counter2].x)+abs(points[1][counter2].y):
        if points[0][counter].x==points[1][counter2].x and points[0][counter].y==points[1][counter2].y and abs(points[0][counter].x)+abs(points[0][counter].y)!=0:
            cross.append(point(abs(points[0][counter].x)+abs(points[0][counter].y),abs(points[1][counter2].x)+abs(points[1][counter2].y)))
            break
        counter2+=1
    counter+=1


for a in cross:
    if a.x+a.y<lowestNumb:
        lowestNumb=a.x+a.y

print(str(len(cross))+", "+str(lowestNumb))




